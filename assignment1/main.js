let stackArray = [];
function saveStack(N, i, r1, r2, r, x, y, picString){
    stackArray.push({ N:N, i:i, r1:r1, r2:r2, r:r, x:x, y:y, picString:picString});
    //console.log(stackArray);
}

function deleteElement(N, i, r1, r2, r, x, y, picString){
    stackArray.pop();
    //console.log(stackArray);
}

function getLatest(){
    return stackArray[stackArray.length-1];
}

function notLoopBody(){
    let i = document.getElementById("index").value;//loop initialization
    let r = 0;
    let N = document.getElementById("N").value;
    let r1 = document.getElementById("r1").value;
    let r2 = document.getElementById("r2").value;
	let x = 0;
	let y = 0;
	let picString = "";
    saveStack(N, i, r1, r2, r, x, y, picString);
}

function loopBody(){
    let lastStackFrame = getLatest();
    let N = lastStackFrame.N;
    let i = lastStackFrame.i;
    let r1 = lastStackFrame.r1;
    let r2 = lastStackFrame.r2;
    
    let r = 0;
    let M = 4;
    let picString = "";
    let svgString = "";
    if(i < eval(M*N)){
        //loop body goes here
        switch (i % M) {
            case 0:
            case 1: r = r1;
                break;
            case 2:
            case 3: r = r2;
                break;
        }
        
        let x = r * Math.cos(i*2*Math.PI/(N*M)) + 100;
        let y = r * Math.sin(i*2*Math.PI/(N*M)) + 100;
        picString += `${x},${y}`;
        svgString=`<polygon points="${picString}" stroke="black"\>`;
        document.querySelector("#svg").innerHTML = svgString;
		
		if(ifForward){
			i++;
			saveStack(N, i, r1, r2, r, x, y, picString);
		}else{
			i--;
			deleteElement(N, i, r1, r2, r, x, y, picString);
		}
        
		//console.log("i:"+i+", r:"+r+", x:"+x+", y:"+y);
    }
 
}

function output(){
	var table = document.getElementById("output");
	table.innerHTML = "<h3>i	r	x	y	picString</h3>";
	
	for(let i=0; i<stackArray.length; i++){
		table.innerHTML += stackArray[i].i+"	";
		table.innerHTML += stackArray[i].r+"	";
		table.innerHTML += stackArray[i].x+"	";
		table.innerHTML += stackArray[i].y+"	";
		table.innerHTML += stackArray[i].picString+"	";
		table.innerHTML += "<br>";
	}
}

let count = 0;   
let ifForward = false;
function forward(){
    if(count==0){
        notLoopBody();
    }
    count++;
	ifForward = true;
    loopBody();
	output();
}

function backward(){
	if(count==0){
        notLoopBody();
    }
    count++;
	ifForward = false;
	loopBody();
	output();
}

function reset(){
	count = 0;
	notLoopBody();
	for(let i=0; i<stackArray.length; i++){
		stackArray.pop();
	}
	output();
}