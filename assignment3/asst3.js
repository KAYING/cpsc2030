




//Creates the two event listenders for the buttones
function listenerFactory() {
   //need this flag, in case people mashes buttons 
   let requestPending = false;
	let itemArray = [];

   //Help to make one request per an item of dateArray
   //Wait till all request is done then update the page
   function makeRequests(count) {
      let promiseArray = [];

      //Loop through date to make requests
      for (let i = 1; i <= 807; i+=20) {

         let URLString = 'https://pokeapi.co/api/v2/pokemon-species + ?offset='+i+'&limit=20';

         let request = new Request(URLString, {
            method: "GET",
            headers: {
               'Content-Type': 'application/json'
            },
            redirect: "follow"
         })

         promiseArray.push(fetch(request)
            .then(function (res) {
               if (!res.ok) {
                  return Promise.reject("Network error");
               }
               return res.json();
            }).then(function (res) {
               //Store the data in the closure variable

               if (res.length == 0) {
                 itemArray[i] = false;  //no draws on these days
                  return Promise.reject("No numbers found")
               } else {
                  itemArray[i] = res[0];  //store object into item array for later use
                  return res;
               }
            }))
      }
      //Promise.allsettles waits for all promises in array
      //to finish, so all fetch operations to finish.
      return Promise.allSettled(promiseArray)
         .then(function (res) {
            //Update the page, once we have all the data
            let element = document.querySelector("#numList")
            element.innerHTML = "";
            let htmlString = "";
			
            for (let i = count; i <= count+20; i++) {
				
               htmlString += '<li>https://pokeapi.co/api/v2/pokemon-species/'+i+'</li>';

            }
            element.innerHTML = htmlString;
         })
         .finally(function () {
            //Unfreeze buttons
            requestPending = false;
         })
   }
	let count=1;
   function next() {
      //Guard against button smashing by user
      //effective locks out buttons, if request are still being made
      if(requestPending){
         return;
      }
      requestPending = true;      
		count+=20;
      makeRequests(count);

   }
   function prev() {
      if(requestPending){
         return;
      }
      requestPending = true;
      count-=20;
      makeRequests(count);

   }
   return { prev: prev, next: next }
}

//Attach event listeners
let listeners = listenerFactory();
document.querySelector("#prev").addEventListener("click", listeners.prev);
document.querySelector("#next").addEventListener("click", listeners.next);